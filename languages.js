/**
 * Language names from Prism.js files
 */
export const LANGUAGE_NAMES = [
  "plaintext",
  "abap",
  "abnf",
  "actionscript",
  "ada",
  "agda",
  "al",
  "antlr4",
  "apacheconf",
  "apex",
  "apl",
  "applescript",
  "aql",
  "arduino",
  "arff",
  "armasm",
  "arturo",
  "asciidoc",
  "asm6502",
  "asmatmel",
  "aspnet",
  "autohotkey",
  "autoit",
  "avisynth",
  "avro-idl",
  "awk",
  "bash",
  "basic",
  "batch",
  "bbcode",
  "bbj",
  "bicep",
  "birb",
  "bison",
  "bnf",
  "bqn",
  "brainfuck",
  "brightscript",
  "bro",
  "bsl",
  "c",
  "cfscript",
  "chaiscript",
  "cil",
  "cilkc",
  "cilkcpp",
  "clike",
  "clojure",
  "cmake",
  "cobol",
  "coffeescript",
  "concurnas",
  "cooklang",
  "coq",
  "cpp",
  "crystal",
  "csharp",
  "cshtml",
  "csp",
  // "css-extras",
  "css",
  "csv",
  "cue",
  "cypher",
  "d",
  "dart",
  "dataweave",
  "dax",
  "dhall",
  "diff",
  "django",
  "dns-zone-file",
  "docker",
  "dot",
  "ebnf",
  "editorconfig",
  "eiffel",
  "ejs",
  "elixir",
  "elm",
  "erb",
  "erlang",
  "etlua",
  "excel-formula",
  "factor",
  "false",
  "firestore-security-rules",
  "flow",
  "fortran",
  "fsharp",
  "ftl",
  "gap",
  "gcode",
  "gdscript",
  "gedcom",
  "gettext",
  "gherkin",
  "git",
  "glsl",
  "gml",
  "gn",
  "go-module",
  "go",
  "gradle",
  "graphql",
  "groovy",
  "haml",
  "handlebars",
  "haskell",
  "haxe",
  "hcl",
  "hlsl",
  "hoon",
  "hpkp",
  "hsts",
  "http",
  "ichigojam",
  "icon",
  "icu-message-format",
  "idris",
  "iecst",
  "ignore",
  "inform7",
  "ini",
  "io",
  "j",
  "java",
  "javadoc",
  "javadoclike",
  "javascript",
  "javastacktrace",
  "jexl",
  "jolie",
  "jq",
  "js-extras",
  "js-templates",
  "jsdoc",
  "json",
  "json5",
  "jsonp",
  "jsstacktrace",
  "jsx",
  "julia",
  "keepalived",
  "keyman",
  "kotlin",
  "kumir",
  "kusto",
  "latex",
  "latte",
  "less",
  "lilypond",
  "linker-script",
  "liquid",
  "lisp",
  "livescript",
  "llvm",
  "log",
  "lolcode",
  "lua",
  "magma",
  "makefile",
  "markdown",
  "markup-templating",
  "markup",
  "mata",
  "matlab",
  "maxscript",
  "mel",
  "mermaid",
  "metafont",
  "mizar",
  "mongodb",
  "monkey",
  "moonscript",
  "n1ql",
  "n4js",
  "nand2tetris-hdl",
  "naniscript",
  "nasm",
  "neon",
  "nevod",
  "nginx",
  "nim",
  "nix",
  "nsis",
  "objectivec",
  "ocaml",
  "odin",
  "opencl",
  "openqasm",
  "oz",
  "parigp",
  "parser",
  "pascal",
  "pascaligo",
  "pcaxis",
  "peoplecode",
  "perl",
  // "php-extras",
  "php",
  "phpdoc",
  "plant-uml",
  "plsql",
  "powerquery",
  "powershell",
  "processing",
  "prolog",
  "promql",
  "properties",
  "protobuf",
  "psl",
  "pug",
  "puppet",
  "pure",
  "purebasic",
  "purescript",
  "python",
  "q",
  "qml",
  "qore",
  "qsharp",
  "r",
  "racket",
  "reason",
  "regex",
  "rego",
  "renpy",
  "rescript",
  "rest",
  "rip",
  "roboconf",
  "robotframework",
  "ruby",
  "rust",
  "sas",
  "sass",
  "scala",
  "scheme",
  "scss",
  "shell-session",
  "smali",
  "smalltalk",
  "smarty",
  "sml",
  "solidity",
  "solution-file",
  "soy",
  "sparql",
  "splunk-spl",
  "sqf",
  "sql",
  "squirrel",
  "stan",
  "stata",
  "stylus",
  "supercollider",
  "swift",
  "systemd",
  "t4-cs",
  "t4-templating",
  "t4-vb",
  "tap",
  "tcl",
  "textile",
  "toml",
  "tremor",
  "tsx",
  "tt2",
  "turtle",
  "twig",
  "typescript",
  "typoscript",
  "unrealscript",
  "uorazor",
  "uri",
  "v",
  "vala",
  "vbnet",
  "velocity",
  "verilog",
  "vhdl",
  "vim",
  "visual-basic",
  "warpscript",
  "wasm",
  "web-idl",
  "wgsl",
  "wiki",
  "wolfram",
  "wren",
  "xeora",
  "xml-doc",
  "xojo",
  "xquery",
  "yaml",
  "yang",
  "zig",
  "gleam",
];

export const LANGUAGES = new Map();

for (let i = 0; i < LANGUAGE_NAMES.length; ++i) {
  LANGUAGES.set(i, LANGUAGE_NAMES[i]);
  LANGUAGES.set(LANGUAGE_NAMES[i], i);
}

export const PLAINTEXT = LANGUAGE_NAMES[0];
export const PATCHED_LANGUAGES = new Set(["gleam"]);

export async function getDepsData() {
  const resp = await fetch("./vendor/prism/components.json");
  const depsData = await resp.json();
  const ret = new Map();
  for (const [lang, data] of Object.entries(depsData.languages)) {
    if ("require" in data) {
      const req = Array.isArray(data.require) ? data.require : [data.require];
      ret.set(lang, req);
    }
  }

  return ret;
}

export function getDependenciesForLanguage(depsData, language) {
  const ret = [];
  const langsToProcess = [language];

  while (langsToProcess.length > 0) {
    const currentLang = langsToProcess.shift();
    const deps = depsData.get(currentLang) ?? [];

    for (const dep of deps) {
      langsToProcess.push(dep);

      // Deps have to be loaded first
      ret.unshift(dep);
    }
  }

  return Array.from(new Set(ret));
}
