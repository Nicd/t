# Tahnaroskakori

Paste = tahna. Bin = roskakori. Pastebin = _tahnaroskakori_!

This is a pastebin that compresses the text contents and adds them to the end of the URL in the hash part. This way no contents need to be sent to the server and the pastebin can just be a static HTML/JS website.

The compression is using [brotli-wasm](https://github.com/httptoolkit/brotli-wasm) in the browser. The compressed data is then encoded with Base66 to be URL safe (percent encoding would just grow the length).

The size that can be pasted is dependent on the URL before the hash. URLs have a 2048 character length limit, so the paste is limited to only generate URLs that fit in that limit.

**Demo: [https://nicd.gitlab.io/t/](https://nicd.gitlab.io/t/)**

Thanks to _daarb_ from the Elixir Discord for the idea and finding brotli-wasm!
